# monnify-ios-sdk

[![Version](https://img.shields.io/cocoapods/v/monnify-ios-sdk.svg?style=flat)](https://cocoapods.org/pods/monnify-ios-sdk)
[![License](https://img.shields.io/cocoapods/l/monnify-ios-sdk.svg?style=flat)](https://cocoapods.org/pods/monnify-ios-sdk)
[![Platform](https://img.shields.io/cocoapods/p/monnify-ios-sdk.svg?style=flat)](https://cocoapods.org/pods/monnify-ios-sdk)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

monnify-ios-sdk is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'Monnify'
```

## Author

Kanyinsola Fapohunda, kfapohunda@teamapt.com

## License

Monnify is available under the MIT license. See the LICENSE file for more info.
