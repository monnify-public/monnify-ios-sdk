//
//  Monnify.swift
//  Monnify
//
//  Created by Kanyinsola on 28/10/2019.
//  Copyright © 2019 TeamApt. All rights reserved.
//

import UIKit.UIViewController

class Monnify : NSObject {
    
    private static let shared = Monnify()
    
    private override init(){
        super.init()
    }
    
    private var applicationMode: ApplicationMode?
    private var environment: Environment?
    private var apiKey: String?
    private var contractCode: String?
    
    func setApplicationMode(applicationMode : ApplicationMode) {
        self.applicationMode = applicationMode
    }
    
    func setApiKey(apiKey: String) {
        self.apiKey = apiKey
    }
    
    func setContractCode(contractCode: String) {
        self.contractCode = contractCode
    }

    func getApplicationMode() -> ApplicationMode? {
        return applicationMode
    }
    
    func getContractCode() -> String? {
        return contractCode
    }
    
    func getApiKey() -> String? {
        return apiKey
    }
    
    func initializePayment(withTransactionParameters parameters : TransactionParameters,
                           presentingViewController viewController: UIViewController,
                           didCompletedWithError errorCompletion : MonnifyErrorCompletion,
                           onTransactionSuccessful successCompletion: MonnifySuccessCompletion) {
        
        // Validate Transaction Parameters.
        
        
        // make call to api.
        
    }
}
