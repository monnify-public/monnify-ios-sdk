//
//  PaymentMethodData.swift
//  Monnify
//
//  Created by Kanyinsola on 25/10/2019.
//  Copyright © 2019 TeamApt. All rights reserved.
//

import UIKit

struct PaymentMethodData {
    
    static let bankTransferPaymentMethod = PaymentMethod(title: "Pay with Bank Transfer",
                                                         subtitle: "Transfer to a Merchants Account",
                                                         icon: UIImage(named: "bank-transfer-icon")!)
    
    static let cardPaymentMethod = PaymentMethod(title: "Pay with Cards",
                                                 subtitle: "Payment with your Debit Card",
                                                 icon: UIImage(named: "pay-with-card-icon")!)
 
    static let paymentMethods = [bankTransferPaymentMethod, cardPaymentMethod]

}
