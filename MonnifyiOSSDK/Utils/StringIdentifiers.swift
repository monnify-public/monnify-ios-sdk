
import Foundation

struct StoryBoardIdentifiers {
    static let Main =                               "Main"
}

struct PersistenceIDs {
    static let AccessToken =                       "access_token"
    static let IsAFirstTimer =                     "is_a_first_timer"
}
