import Foundation

class FileUtils {
    
    static func readHTMLResourceFile(fileName: String) -> String {
        Logger.log("readHTMLResourceFile \(fileName)")
        var html = ""
        if let htmlPathURL = Bundle.main.url(forResource: fileName,
            withExtension: "html"){
            Logger.log("Bundle.main.url \(htmlPathURL)")

            do {
                html = try String(contentsOf: htmlPathURL, encoding: .utf8)
            } catch  {
                Logger.log("Unable to get the file.")
            }
        } else {
            Logger.log("Couldn't find the file.")
        }
        return html
    }
}
