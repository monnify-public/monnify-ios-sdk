//
//  TypeAliases.swift
//  Monnify
//
//  Created by Kanyinsola on 28/10/2019.
//  Copyright © 2019 TeamApt. All rights reserved.
//

import Foundation

typealias MonnifyErrorCompletion = (_ error: Error) -> Void
typealias MonnifySuccessCompletion = () -> Void
