import Foundation
import Alamofire
import SwiftyJSON

public class Network {
    
    public static let shared = Network()
    
    func getDefaultHeaders() -> HTTPHeaders {
        var headers = ["Content-Type": "application/json"]

        if let token = LocalStorage.shared.getAccessToken() {
            headers[ApiConstants.Authorization] = "Bearer \(token)"
        }
        
        return headers
    }
    
    private func getFinalHeaders(_ headers: HTTPHeaders) -> HTTPHeaders {
        var finalHeaders = getDefaultHeaders()
        for (key, value) in headers {
            finalHeaders[key] = value
        }
        return finalHeaders
    }
    
    public func request(_ urlString: String,
                               method: HTTPMethod = .get,
                               parameters: Parameters? = nil,
                               headers: HTTPHeaders = [:],
                               completion: @escaping (_ response: JSON?) -> Void) {
        let url = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!

        Alamofire.request(URL(string: url)!,
                         method: method,
                         parameters: parameters,
                         encoding: JSONEncoding.default,
                         headers: getFinalHeaders(headers))
            .debugLog()
            .responseJSON { (response) in
                
                switch response.result {
                    case .success(let value):
                        let json = JSON(value)
                        completion(json)
                    case .failure(let error):
                        completion(nil)
                        Logger.log(error)
                }
        }
    }
}
