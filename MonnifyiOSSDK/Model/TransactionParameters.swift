//
//  TransactionParameters.swift
//  Monnify
//
//  Created by Kanyinsola on 28/10/2019.
//  Copyright © 2019 TeamApt. All rights reserved.
//

import Foundation

struct TransactionParameters {
    let amount : Decimal
    let currencyCode: String
    let paymentReference: String
    let customerEmail: String
    let customerName: String?
    let customerMobileNumber : String?
    let paymentDescription: String?
    let incomeSplitConfig: [SubAccountDetails]?
    let tokeniseCard: Bool?
}
