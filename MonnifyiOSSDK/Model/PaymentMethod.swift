//
//  PaymentMethod.swift
//  Monnify
//
//  Created by Kanyinsola on 25/10/2019.
//  Copyright © 2019 TeamApt. All rights reserved.
//

import UIKit

struct PaymentMethod {
    let title: String
    let subtitle: String
    let icon: UIImage
}
